<?php

/**
 * Implementation of hook_context_default_contexts().
 */
function oa_webform_context_default_contexts() {
  $export = array();
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'oa_custom_webform';
  $context->description = '';
  $context->tag = 'Open Atrium Webform';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'webform' => 'webform',
        'webform_report' => 'webform_report',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'views' => array(
      'values' => array(
        'oa_custom_webform' => 'oa_custom_webform',
        'oa_custom_webform:page_1' => 'oa_custom_webform:page_1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(),
      'layout' => 'wide',
    ),
    'menu' => 'features',
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('OA Webform');

  $export['oa_custom_webform'] = $context;
  return $export;
}
