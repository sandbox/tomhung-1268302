<?php

/**
 * Implementation of hook_menu_default_menu_links().
 */
function oa_webform_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: features:forms
  $menu_links['features:forms'] = array(
    'menu_name' => 'features',
    'link_path' => 'forms',
    'router_path' => 'forms',
    'link_title' => 'Forms',
    'options' => array(),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Forms');


  return $menu_links;
}
